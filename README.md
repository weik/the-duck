# theduckproject

A new Flutter web application for practice 2020.

It was developed by 4 students of Lviv Polytechnic National University.
 
This "project" is all about ones laziness during quarantine caused by COVID-19.

On the home page, you can spin the button and look for activities you can do during quarantine 

On the support page you can write your indignation and send it to us, we will find a solution for your problem to achieve negotiation ^-^
 
 Link to the project below:
 
 - [The Duck Team](https://the-duck-project.codemagic.app/#/)



If you are interested in technologies we were using, there are some useful links : 

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)


And of course the perfect Flutter documentation:

[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
