import 'package:flutter/material.dart';

import '../screens/about_us_page/about_us_page.dart';
import '../screens/add_page/add_page.dart';
import '../screens/home_page/home_page.dart';
import '../screens/support_page/support_page.dart';

const String home_page = '/';
const String add_page = '/add';
const String support_page = '/support';
const String about_us_page = '/about';

class Router {
  static PageRoute<dynamic> generateRoute(RouteSettings settings) {
    Widget child;
    switch (settings.name) {
      case home_page:
        child = HomePage();
        break;
      case add_page:
        child = AddPage();
        break;
      case support_page:
        child = SupportPage();
        break;
      case about_us_page:
        child = AboutUsPage();
        break;
    }
    return PageRouteBuilder(
      settings: settings,
      pageBuilder: (_, __, ___) => child,
      transitionsBuilder: (_, animation, ___, ____) {
        return FadeTransition(
          opacity: animation,
          child: child,
        );
      },
    );
  }
}
