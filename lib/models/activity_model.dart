import 'dart:convert';
import 'dart:typed_data';

class Activity {
  final String name;
  final String description;
  final String url;
  final Uint8List imageBytes;

  Activity({this.name, this.description, this.url, this.imageBytes});

  Activity.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        description = json['description'],
        url = json['url'],
        imageBytes = _convertImageStringToBytes(json['img']);

  Map<String, dynamic> toJson(List<String> categories) => {
        'name': name,
        'description': description,
        'url': url,
        'img': _convertImageBytesToString(imageBytes),
        'categories': categories,
      };

  static Uint8List _convertImageStringToBytes(String source) =>
      base64Decode(source.substring(23));

  static String _convertImageBytesToString(Uint8List source) =>
      'data:image/jpeg;base64,' + base64Encode(source).toString();
}
