import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SendMessageSupport extends ChangeNotifier {
  List<String> attachments = [];

  bool _isDispose = false;

  final recipientTextController = TextEditingController(
    text: 'lyubomyr2030@gmail.com',
  );
  final subjectTextController = TextEditingController();
  final descriptionTextController = TextEditingController();

  final GlobalKey<ScaffoldState> supportBigPageKey = GlobalKey<ScaffoldState>();

  final GlobalKey<ScaffoldState> supportSmallPageKey =
      GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  /// Start of [VALIDATION]
  String validateSubject(String value) {
    if (value.isEmpty) {
      return 'You have to provide a small description of your problem';
    }
    return null;
  }

  ///
  String validateDescription(String value) {
    if (value.isEmpty) {
      return 'You have to provide a description';
    }
    return null;
  }

  ///End of [VALIDATION]
  ///
  ///Sending messages [START]
  _sendMessage() async {
    String platformResponse;
    print('1');
    final String _email = 'mailto:' +
        recipientTextController.text +
        '?subject=' +
        subjectTextController.text +
        '&body=' +
        descriptionTextController.text;
    try {
      await launch(_email);
      print('2');
      platformResponse = 'success';
    } catch (e) {
      platformResponse = e.toString();
    }
    print('3');
    if (!_isDispose) return;
    supportBigPageKey.currentState.showSnackBar(SnackBar(
      content: Text(platformResponse),
    ));
    supportSmallPageKey.currentState.showSnackBar(SnackBar(
      content: Text(platformResponse),
    ));

    print('maybe sent');
    notifyListeners();
  }

  void sendMessage() {
    if (formKey.currentState.validate()) {
      _sendMessage();
      print('message sent');
      subjectTextController.text = '';
      descriptionTextController.text = '';
    }
  }

  ///Sending messages [END]
  ///
  ///
  /// [DISPOSE]
  @override
  void dispose() {
    descriptionTextController.dispose();
    subjectTextController.dispose();
    recipientTextController.dispose();
    super.dispose();
    _isDispose = true;
  }
}
