import 'package:flutter/material.dart';

import '../models/activity_model.dart';
import '../services/spin_service.dart';

class HomePageLogic extends ChangeNotifier {
  List<Activity> activities = [];

  String category = '';

  void spin() async {
    try {
      final Map<String, dynamic> initialActivities =
          await SpinService.spinActivities();
      category = initialActivities['category'];
      activities = initialActivities['activities'];
    } catch (e) {
      print(e);
    }
    notifyListeners();
  }
}
