import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/activity_model.dart';

///Temp mock to retrieve fake [Activities] from scratch
const tempSpin = 'https://run.mocky.io/v3/f866a4d6-22c2-4bb6-8475-07d285a00401';

///Temp mock to retrieve fake [Activities] within given category
const tempLoad = 'https://run.mocky.io/v3/d90f04d4-19d6-43d5-a142-d6e0ba18a998';

class SpinService {
  static final String _apiUrl = 'https://the-duck-spinner.herokuapp.com';
  static final String _spinUrl = '$_apiUrl/spin';
  static final String _spinMoreActivitiesUrl = '$_apiUrl/activities';
  static final String _addActivityUtl = '$_apiUrl/activity';

  static Future<Map<String, dynamic>> spinActivities() async {
    final response = await http.get(_spinUrl,
        headers: {'Content-Type': 'application/json; charset=UTF-8'});
    if (response.statusCode == 200) {
      final result = jsonDecode(response.body);
      if (result == null) {
        throw 'Body is null';
      } else if (result['activities'] == []) {
        throw 'There is no activities in this category';
      } else if (result != null) {
        return {
          'category': result['category'],
          'activities': _loadActivities(result['activities'])
        };
      }
    } else {
      throw response.statusCode;
    }
  }

  static Future<List<Activity>> loadActivities(String category) async {
    final response = await http.get('$_spinMoreActivitiesUrl/$category',
        headers: {'Content-Type': 'application/json; charset=UTF-8'});
    if (response.statusCode == 200) {
      final decodedData = jsonDecode(response.body);
      return _loadActivities(decodedData['activities']);
    } else {
      throw response.statusCode;
    }
  }

  static Future<void> uploadActivity(
      Activity activity, List<String> categories) async {
    final jsonString = jsonEncode(activity.toJson(categories));
    final response = await http.post(_addActivityUtl,
        headers: {'Content-Type': 'application/json; charset=UTF-8'},
        body: jsonString);
    if (response.statusCode < 400) {
      print('good');
    } else {
      throw response.statusCode;
    }
  }

  static List<Activity> _loadActivities(List jsonList) {
    List<Activity> activities = [];
    for (final jsonActivity in jsonList) {
      activities.add(Activity.fromJson(jsonActivity));
    }
    return activities;
  }
}
