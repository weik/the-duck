import 'package:flutter/material.dart';

class CategoryCard extends StatelessWidget {
  final String category;

  CategoryCard({@required this.category});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxHeight: 100,
        maxWidth: 200,
        minHeight: 50,
        minWidth: 50,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image(
              image: AssetImage('assets/category_background.jpg'),
              fit: BoxFit.fill,
            ),
            Center(
              child: Text(
                category,
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.deepOrange.withOpacity(0.8),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
