import 'package:flutter/material.dart';
import 'package:theduckproject/models/activity_model.dart';

import '../../../services/spin_service.dart';
import 'activity_card.dart';

class ActivityList extends StatefulWidget {
  final String category;
  final List<Activity> initialActivities;
  final bool shrinkWrap;

  ActivityList({this.category, this.initialActivities, this.shrinkWrap});

  @override
  _ActivityListState createState() => _ActivityListState();
}

class _ActivityListState extends State<ActivityList> {
  List<ActivityCard> activityCards = [];

  void _addActivities(List<Activity> newActivities) {
    for (final activity in newActivities) {
      activityCards.add(ActivityCard(
        name: activity.name,
        description: activity.description,
        imageBytes: activity.imageBytes,
        url: activity.url,
      ));
    }
  }

  void _loadMore() async {
    try {
      final newActivities = await SpinService.loadActivities(widget.category);
      setState(() {
        _addActivities(newActivities);
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void didUpdateWidget(ActivityList oldWidget) {
    activityCards.clear();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (activityCards.isEmpty) {
      _addActivities(widget.initialActivities);
    }

    return ListView.builder(
      shrinkWrap: widget.shrinkWrap,
      itemCount: activityCards.length + 1,
      itemBuilder: (_, index) {
        return index == activityCards.length
            ? FlatButton(
                child: Text(
                  'Load more',
                  style: TextStyle(color: Colors.deepOrange.withOpacity(0.7)),
                ),
                onPressed: _loadMore,
              )
            : activityCards[index];
      },
      physics: BouncingScrollPhysics(),
    );
  }
}
