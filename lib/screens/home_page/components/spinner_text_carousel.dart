import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';

class TextCarousel extends StatefulWidget {
  @override
  _TextCarouselState createState() => _TextCarouselState();
}

class _TextCarouselState extends State<TextCarousel> {
  final List<String> carouselText = [
    'Don`t know why you are here?',
    'Don`t know how to spend your time?',
    'Then quick press the button below!!!'
  ];

  @override
  Widget build(BuildContext context) {
    return GFCarousel(
      height: 75,
      autoPlay: true,
      viewportFraction: 1,
      autoPlayInterval: Duration(seconds: 3),
      items: carouselText.map((text) {
        return Container(
          width: 300,
          margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.0,
                0.5,
                1,
              ],
              colors: [
                Colors.white10.withOpacity(0.1),
                Colors.grey[600].withOpacity(0.2),
                Colors.white10.withOpacity(0.1),
              ],
            ),
          ),
          constraints: BoxConstraints(
            minHeight: 20,
            maxHeight: 60,
            minWidth: 50,
            maxWidth: 300,
          ),
          child: ClipRRect(
            child: Center(
              child: Text(
                text,
                style: TextStyle(
                  color: Colors.white70,
                  fontWeight: FontWeight.w500,
                  fontSize: 15,
                ),
              ),
            ),
          ),
        );
      }).toList(),
      onPageChanged: (index) {
        setState(() {
          index;
        });
      },
    );
  }
}
