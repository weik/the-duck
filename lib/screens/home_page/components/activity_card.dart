import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ActivityCard extends StatelessWidget {
  final Uint8List imageBytes;
  final String name;
  final String description;
  final String url;

  ActivityCard(
      {@required this.imageBytes,
      @required this.name,
      @required this.description,
      @required this.url});

  @override
  Widget build(BuildContext context) {
    return Align(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black45,
            borderRadius: BorderRadius.circular(10),
          ),
          height: 200,
          constraints: BoxConstraints(
            maxWidth: 600,
            minWidth: 400,
          ),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: 200,
                  width: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    image: DecorationImage(
                      image: MemoryImage(imageBytes),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ListView(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        name,
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.deepOrange.withOpacity(0.6),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        description,
                        style: TextStyle(fontSize: 16, color: Colors.grey),
                      ),
                    ),
                    url != null
                        ? Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 5,
                            ),
                            child: RaisedButton(
                              hoverColor: Colors.deepOrange.withOpacity(0.3),
                              color: Colors.white10.withOpacity(0.1),
                              child: Text('GoTo'),
                              onPressed: () async {
                                if (await canLaunch(url)) {
                                  await launch(
                                    url,
                                    forceSafariVC: false,
                                    forceWebView: false,
                                  );
                                } else {
                                  print('can\'t launch $url');
                                }
                              },
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
