import 'package:flutter/material.dart';

import 'big_home_page.dart';
import 'small_home_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        if (constraints.maxWidth < 1200 || constraints.maxHeight < 800) {
          return SmallHomePage();
        } else {
          return BigHomePage();
        }
      },
    );
  }
}
