import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:theduckproject/provider/home_page_logic.dart';

import '../../components/small_custom_footer.dart';
import 'components/activity_list.dart';
import 'components/category_card.dart';
import 'components/spin_button.dart';
import 'components/spinner_text_carousel.dart';

class SmallHomePage extends StatefulWidget {
  @override
  _SmallHomePageState createState() => _SmallHomePageState();
}

class _SmallHomePageState extends State<SmallHomePage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<HomePageLogic>(
      builder: (_, homePage, child) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 1,
          ),
          Flexible(
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 450,
                      height: 100,
                      child: TextCarousel(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SpinButton(
                      onPressed: homePage.spin,
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                homePage.category.isNotEmpty
                    ? Center(
                        child: CategoryCard(
                          category: homePage.category,
                        ),
                      )
                    : SizedBox(),
                SizedBox(
                  height: 30,
                ),
                homePage.activities.isNotEmpty
                    ? Container(
                        width: 400,
                        child: ActivityList(
                          shrinkWrap: true,
                          category: homePage.category,
                          initialActivities: homePage.activities,
                        ),
                      )
                    : SizedBox(),
                SizedBox(
                  height: 30,
                ),
                homePage.activities.isEmpty ? SizedBox() : SmallCustomFooter(),
              ],
            ),
          ),
          homePage.activities.isEmpty ? SmallCustomFooter() : SizedBox(),
        ],
      ),
    );
  }
}
