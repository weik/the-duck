import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:theduckproject/provider/home_page_logic.dart';

import '../../components/big_custom_footer.dart';
import 'components/activity_list.dart';
import 'components/category_card.dart';
import 'components/spin_button.dart';
import 'components/spinner_text_carousel.dart';

class BigHomePage extends StatefulWidget {
  @override
  _BigHomePageState createState() => _BigHomePageState();
}

class _BigHomePageState extends State<BigHomePage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(),
        Consumer<HomePageLogic>(
          builder: (_, homePage, child) => Row(
            mainAxisAlignment: homePage.activities.isNotEmpty
                ? MainAxisAlignment.spaceAround
                : MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: 450,
                    height: 100,
                    child: TextCarousel(),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SpinButton(
                    onPressed: homePage.spin,
                  ),
                ],
              ),
              homePage.category.isNotEmpty
                  ? CategoryCard(
                      category: homePage.category,
                    )
                  : SizedBox(),
              homePage.activities.isNotEmpty
                  ? Container(
                      width: 500,
                      height: 700,
                      child: ActivityList(
                        category: homePage.category,
                        initialActivities: homePage.activities,
                        shrinkWrap: false,
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ),
        BigCustomFooter(),
      ],
    );
  }
}
