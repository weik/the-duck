import 'package:flutter/material.dart';

import '../../routing/router.dart';
import '../../components/end_drawer.dart';
import 'components/custom_navigator.dart';
import 'components/small_header.dart';

class SmallMainPage extends StatelessWidget {
  final Widget child;
  final GlobalKey<NavigatorState> navigatorKey;

  SmallMainPage({this.child, this.navigatorKey});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SmallHeader(
        homeButtonCallback: () =>
            navigatorKey.currentState.pushNamed(home_page),
      ),
      body: SafeArea(
        child: CustomNavigator(
          navigatorKey: navigatorKey,
        ),
      ),
      endDrawer: EndDrawer(
        navigatorKey: navigatorKey,
      ),
    );
  }
}
