import 'package:flutter/material.dart';

import '../../screens/main_page/big_main_page.dart';
import '../../screens/main_page/small_main_page.dart';

class MainPage extends StatelessWidget {
  final Widget child;
  MainPage({this.child});

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        if (constraints.maxWidth < 1200 || constraints.maxHeight < 800) {
          return SmallMainPage(
            child: child,
            navigatorKey: navigatorKey,
          );
        } else {
          return BigMainPage(
            child: child,
            navigatorKey: navigatorKey,
          );
        }
      },
    );
  }
}
