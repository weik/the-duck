import 'package:flutter/material.dart';

import '../../routing/router.dart';
import '../../screens/main_page/components/custom_navigator.dart';
import 'components/big_header.dart';

class BigMainPage extends StatelessWidget {
  final Widget child;
  final GlobalKey<NavigatorState> navigatorKey;

  BigMainPage({this.child, this.navigatorKey});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BigHeader(
        homeButtonCallback: () =>
            navigatorKey.currentState.pushNamed(home_page),
        addButtonCallback: () => navigatorKey.currentState.pushNamed(add_page),
        aboutUsButtonCallback: () =>
            navigatorKey.currentState.pushNamed(about_us_page),
        supportButtonCallback: () =>
            navigatorKey.currentState.pushNamed(support_page),
      ),
      body: SafeArea(
          child: CustomNavigator(
        navigatorKey: navigatorKey,
      )),
    );
  }
}
