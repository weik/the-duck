import 'package:flutter/material.dart';

class SmallHeader extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize; // default value is 56.0

  final Function homeButtonCallback;

  SmallHeader({Key key, @required this.homeButtonCallback})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.black87,
      centerTitle: false,
      leading: InkWell(
        focusColor: Colors.black87,
        highlightColor: Colors.black87,
        splashColor: Colors.black87,
        hoverColor: Colors.black87,
        onTap: homeButtonCallback,
        child: Container(
          height: 40,
          child: Image(
            image: AssetImage('assets/ducklogo.png'),
            fit: BoxFit.fill,
          ),
        ),
      ),
      title: Text(
        'The Duck Team',
        style: TextStyle(
          fontSize: 18,
          color: Colors.white54,
        ),
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.menu),
          onPressed: () => Scaffold.of(context).openEndDrawer(),
        )
      ],
    );
  }
}
