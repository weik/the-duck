import 'package:flutter/material.dart';

import 'header_action_button.dart';

class HeaderActions extends StatelessWidget {
  final Function addButtonCallback;
  final Function supportButtonCallback;
  final Function aboutUsButtonCallback;

  HeaderActions(
      {@required this.addButtonCallback,
      @required this.supportButtonCallback,
      @required this.aboutUsButtonCallback});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black54,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          /* these row contains our header buttons */
          Align(
            alignment: Alignment.centerRight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                  child: HeaderActionButton(
                    text: 'Add',
                    onPressed: addButtonCallback,
                    color: Colors.black12,
                    hoverColor: Colors.blueGrey,
                    borderSideColor: Colors.blueGrey,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 10, 30, 10),
                  child: HeaderActionButton(
                    text: 'About Us',
                    onPressed: aboutUsButtonCallback,
                    color: Colors.black12,
                    hoverColor: Colors.red[800],
                    borderSideColor: Colors.red,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 10, 60, 10),
                  child: HeaderActionButton(
                    text: 'Support',
                    onPressed: supportButtonCallback,
                    color: Colors.black12,
                    hoverColor: Colors.red[800],
                    borderSideColor: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
