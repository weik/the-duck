import 'package:flutter/material.dart';

import 'header_actions.dart';

class BigHeader extends AppBar {
  final Function homeButtonCallback;
  final Function addButtonCallback;
  final Function supportButtonCallback;
  final Function aboutUsButtonCallback;

  BigHeader(
      {Key key,
      this.homeButtonCallback,
      this.addButtonCallback,
      this.supportButtonCallback,
      this.aboutUsButtonCallback})
      : super(
          key: key,
          backgroundColor: Colors.black87,
          centerTitle: false,
          leading: InkWell(
            focusColor: Colors.black87,
            highlightColor: Colors.black87,
            splashColor: Colors.black87,
            hoverColor: Colors.black87,
            onTap: homeButtonCallback,
            child: Container(
              height: 40,
              child: Image(
                image: AssetImage('assets/ducklogo.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          title: Container(
            child: Text(
              'The Duck Team',
              style: TextStyle(
                fontSize: 18,
                color: Colors.white54,
              ),
            ),
          ),
          actions: [
            HeaderActions(
              addButtonCallback: addButtonCallback,
              aboutUsButtonCallback: aboutUsButtonCallback,
              supportButtonCallback: supportButtonCallback,
            ),
          ],
        );
}
