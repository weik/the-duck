import 'package:flutter/material.dart';

import '../../../routing/router.dart';

class CustomNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;

  CustomNavigator({this.navigatorKey});

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      initialRoute: home_page,
      onGenerateRoute: Router.generateRoute,
    );
  }
}
