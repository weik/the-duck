import 'package:flutter/material.dart';

import 'big_about_us_page.dart';
import 'small_about_us_page.dart';

class AboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        if (constraints.maxWidth < 1200 || constraints.maxHeight < 800) {
          return SmallAboutUsPage();
        } else {
          return BigAboutUsPage();
        }
      },
    );
  }
}
