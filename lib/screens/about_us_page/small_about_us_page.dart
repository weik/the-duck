import 'package:flutter/material.dart';

import '../../components/small_custom_footer.dart';
import 'components/about_us_block_description.dart';
import 'components/about_us_carousel.dart';

class SmallAboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        AboutUsCarousel(),
        SizedBox(
          height: 30,
        ),
        BlockDescription(
          imageOverlay: 'assets/beer_lyubomir.jpg',
          imageAvatar: 'assets/lyubomir.jpg',
          name: 'Lyubomir Ishchenko',
          description: 'Flutter Developer\nLinkedIn:@lyubomir-ishchenko',
          content: 'Hello, our group have created',
        ),
        SizedBox(
          height: 30,
        ),
        BlockDescription(
          imageOverlay: 'assets/beer.jpg',
          imageAvatar: 'assets/yarik.jpeg',
          name: 'Yaroslav Siomka',
          description: 'Flutter Developer\nLinkedIn:@yarik-siomka',
          content: 'that amazing website to make',
        ),
        SizedBox(
          height: 30,
        ),
        BlockDescription(
          imageOverlay: 'assets/wine.jpg',
          imageAvatar: 'assets/dana.jpeg',
          name: 'Dana Dobushovska',
          description: 'Automation QA\nLinkedIn:@dana-dobushovska',
          content: 'your life and choice easier,',
        ),
        SizedBox(
          height: 30,
        ),
        BlockDescription(
          imageOverlay: 'assets/whiskey.jpg',
          imageAvatar: 'assets/alexandr.jpeg',
          name: 'Alexandr Mamro',
          description: 'Full-stack-pro Developer\nLinkedIn:@alexandr-mamro',
          content: 'because you are lazy to choose.',
        ),
        SizedBox(
          height: 30,
        ),
        SmallCustomFooter(),
      ],
    );
  }
}
