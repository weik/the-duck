import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';

class AboutUsCarousel extends StatefulWidget {
  @override
  _AboutUsCarouselState createState() => _AboutUsCarouselState();
}

class _AboutUsCarouselState extends State<AboutUsCarousel> {
  final List<String> textCarousel = [
    'Haven`t heard about us?',
    'Want to know more about the developers?',
    'For that and more just look below!',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        minWidth: 100,
        maxWidth: 900,
      ),
      child: GFCarousel(
        height: 100,
        viewportFraction: 1,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        items: textCarousel.map((text) {
          return Container(
            margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [
                  0.0,
                  0.2,
                  0.4,
                  0.5,
                  0.6,
                  0.8,
                  1.0,
                ],
                colors: [
                  Colors.white10.withOpacity(0.1),
                  Colors.black12.withOpacity(0.1),
                  Colors.black12.withOpacity(0.2),
                  Colors.black12.withOpacity(0.2),
                  Colors.black12.withOpacity(0.2),
                  Colors.black12.withOpacity(0.1),
                  Colors.white10.withOpacity(0.1),
                ],
              ),
            ),
            constraints: BoxConstraints(
              minHeight: 20,
              maxHeight: 60,
              minWidth: 300,
              maxWidth: 700,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Center(
                child: Text(
                  text,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          );
        }).toList(),
        onPageChanged: (index) {
          setState(() {
            index;
          });
        },
      ),
    );
  }
}
