import 'package:flutter/material.dart';

import '../../components/big_custom_footer.dart';
import 'components/about_us_block_description.dart';
import 'components/about_us_carousel.dart';

class BigAboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        AboutUsCarousel(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                BlockDescription(
                  imageOverlay: 'assets/beer_lyubomir.jpg',
                  imageAvatar: 'assets/lyubomir.jpg',
                  name: 'Lyubomir Ishchenko',
                  description:
                      'Flutter Developer\nLinkedIn:@lyubomir-ishchenko',
                  content: 'Hello, our group have created',
                ),
                SizedBox(
                  height: 100,
                ),
                BlockDescription(
                  imageOverlay: 'assets/wine.jpg',
                  imageAvatar: 'assets/dana.jpeg',
                  name: 'Dana Dobushovska',
                  description: 'Automation QA\nLinkedIn:@dana-dobushovska',
                  content: 'your life and choice easier,',
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                BlockDescription(
                  imageOverlay: 'assets/beer.jpg',
                  imageAvatar: 'assets/yarik.jpeg',
                  name: 'Yaroslav Siomka',
                  description: 'Flutter Developer\nLinkedIn:@yarik-siomka',
                  content: 'that amazing website to make',
                ),
                SizedBox(
                  height: 100,
                ),
                BlockDescription(
                  imageOverlay: 'assets/whiskey.jpg',
                  imageAvatar: 'assets/alexandr.jpeg',
                  name: 'Alexandr Mamro',
                  description:
                      'Full-stack-pro Developer\nLinkedIn:@alexandr-mamro',
                  content: 'because you are lazy to choose.',
                ),
              ],
            ),
          ],
        ),
        BigCustomFooter(),
      ],
    );
  }
}
