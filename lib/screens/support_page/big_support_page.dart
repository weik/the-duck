// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../components/big_custom_footer.dart';
import '../../components/big_custom_text_field.dart';
import '../../provider/support_page_logic.dart';

class BigSupportPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<SendMessageSupport>(
      builder: (context, sendMessage, child) => Column(
        key: sendMessage.supportBigPageKey,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.black26,
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  stops: [
                    0.0,
                    0.2,
                    0.4,
                    0.6,
                    0.8,
                    1,
                  ],
                  colors: [
                    Colors.black12,
                    Colors.black26,
                    Colors.black38,
                    Colors.black38,
                    Colors.black26,
                    Colors.black12,
                  ],
                ),
              ),
              constraints: BoxConstraints(
                minHeight: 50,
                maxHeight: 70,
                minWidth: 100,
                maxWidth: MediaQuery.of(context).size.width / 2,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 1,
                  ),
                  Container(
                    height: 60,
                    width: 70,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage('assets/ducklogo.png'),
                          fit: BoxFit.cover),
                    ),
                  ),
                  Text(
                    'Just Do  IT !!',
                    style: TextStyle(
                        color: Colors.deepOrange.withOpacity(0.7),
                        fontSize: 22,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Form(
                key: sendMessage.formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    BigCustomTextField(
                      controller: sendMessage.recipientTextController,
                      maxLines: 1,
                      labelText: 'Recipient',
                      enabled: false,
                    ),
                    BigCustomTextField(
                      validator: sendMessage.validateSubject,
                      controller: sendMessage.subjectTextController,
                      maxLines: 1,
                      labelText: 'Subject',
                      hintText: 'Text your problem overall...',
                    ),
                    BigCustomTextField(
                      validator: sendMessage.validateDescription,
                      controller: sendMessage.descriptionTextController,
                      maxLines: 6,
                      labelText: 'Describe your problem more clearly',
                    ),
                    ...sendMessage.attachments.map(
                      (item) => Text(
                        item,
                        overflow: TextOverflow.visible,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: FloatingActionButton.extended(
                        autofocus: true,
                        icon: Icon(Icons.send),
                        label: Text(
                          'Send',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        backgroundColor: Colors.black12,
                        hoverColor: Colors.deepOrange.withOpacity(0.5),
                        onPressed: () {
                          sendMessage.sendMessage();
                        },
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          BigCustomFooter(),
        ],
      ),
    );
  }
}
