import 'package:flutter/material.dart';

import 'big_support_page.dart';
import 'small_support_page.dart';

class SupportPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        if (constraints.maxWidth < 1200 || constraints.maxHeight < 800) {
          return SmallSupportPage();
        } else {
          return BigSupportPage();
        }
      },
    );
  }
}
