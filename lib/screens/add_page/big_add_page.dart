import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker_web/image_picker_web.dart';

import '../../components/big_custom_footer.dart';
import '../../components/big_custom_text_field.dart';
import '../../models/activity_model.dart';
import '../../services/spin_service.dart';

class BigAddPage extends StatefulWidget {
  @override
  _BigAddPageState createState() => _BigAddPageState();
}

class _BigAddPageState extends State<BigAddPage> {
  final _formKey = GlobalKey<FormState>();

  final _categoryTextController = TextEditingController();
  final _nameTextController = TextEditingController();
  final _descriptionTextController = TextEditingController();
  final _urlTextController = TextEditingController();
  Uint8List _imageBytes;

  ///Temporary solution for validating picked image
  ///
  ///All usages of [_imageHintText] will be reworked soon c:
  String _imageHintText = 'Choose an image for your activity';

  String _validateCategory(String value) {
    if (value.isEmpty) {
      return 'You have to input at least 1 category';
    } else {
      final notSymbolPattern =
          '[0-9]|[!@#\$%&*()_+=|<>?{}\\[\`\^\'\"\;\:\/\.\,~-]';
      final notSymbolRegExp = RegExp('$notSymbolPattern', caseSensitive: false);
      final match = notSymbolRegExp.firstMatch(value);
      if (match != null) {
        return 'Category can\`t contain any numbers or special characters';
      }
    }
    return null;
  }

  String _validateName(String value) {
    if (value.isEmpty) {
      return 'You have to give a name for your activity';
    }
    return null;
  }

  String _validateDescription(String value) {
    if (value.isEmpty) {
      return 'You can\`t leave the description empty';
    }
    return null;
  }

  String _validateUrl(String value) {
    if (value.isNotEmpty) {
      final urlPattern =
          r'(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})';
      final urlRegExp = RegExp('$urlPattern', caseSensitive: false);
      final match = urlRegExp.firstMatch(value);
      if (match == null) {
        return 'Invalid url';
      }
    }
    return null;
  }

  void _pickImage() async {
    final imageFromPicker =
        await ImagePickerWeb.getImage(outputType: ImageType.bytes);

    if (imageFromPicker != null) {
      setState(() {
        _imageBytes = imageFromPicker;
        _imageHintText = 'Cool image bro!';
      });
    }
  }

  void _submitActivity() async {
    if (_imageBytes == null) {
      setState(() {
        _imageHintText = 'You have to upload image to proceed';
      });
      return;
    }
    if (_formKey.currentState.validate()) {
      final newActivity = Activity(
        name: _nameTextController.text,
        description: _descriptionTextController.text,
        imageBytes: _imageBytes,
        url:
            _urlTextController.text.isNotEmpty ? _urlTextController.text : null,
      );

      final categoriesList = _categoryTextController.text.split(',');
      await SpinService.uploadActivity(newActivity, categoriesList);

      _nameTextController.text = '';
      _descriptionTextController.text = '';
      _categoryTextController.text = '';
      _urlTextController.text = '';
      setState(() {
        _imageBytes = null;
        _imageHintText = 'Choose an image for your activity';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  BigCustomTextField(
                    validator: _validateCategory,
                    controller: _categoryTextController,
                    labelText: 'Category',
                    hintText:
                        'What is the category of your activity? (If multiple, separate with \"\,\")',
                    maxLines: 1,
                  ),
                  BigCustomTextField(
                    validator: _validateName,
                    controller: _nameTextController,
                    labelText: 'Name',
                    hintText: 'Give it a name',
                    maxLines: 1,
                  ),
                  BigCustomTextField(
                    validator: _validateDescription,
                    controller: _descriptionTextController,
                    labelText: 'Description',
                    hintText: 'Put your description here',
                    maxLines: null,
                    maxLength: 200,
                    //      padding: EdgeInsets.all(10),
                  ),
                  Text(
                    _imageHintText,
                    style: TextStyle(color: Colors.white60),
                  ),
                  Image(
                    image: _imageBytes != null
                        ? MemoryImage(_imageBytes)
                        : AssetImage('assets/ducklogo.png'),
                    width: 150,
                    height: 150,
                  ),
                  FlatButton(
                    child: Text(
                      'Open image',
                      style: TextStyle(color: Colors.white60),
                    ),
                    onPressed: _pickImage,
                  ), // a
                  BigCustomTextField(
                    validator: _validateUrl,
                    controller: _urlTextController,
                    //height: 70,
                    labelText: 'Url (optional)',
                    hintText: 'Drop the link to check out your activity',
                    //  padding: EdgeInsets.all(10),
                    maxLines: 1,
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(
                        color: Colors.deepOrange.withOpacity(0.6),
                        width: 2,
                      ),
                    ),
                    color: Colors.white10,
                    hoverColor: Colors.deepOrange.withOpacity(0.5),
                    child: Text(
                      'Submit',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                    onPressed: _submitActivity,
                  ),
                ],
              ),
            ),
          ],
        ),
        BigCustomFooter(),
      ],
    );
  }

  @override
  void dispose() {
    _categoryTextController.dispose();
    _descriptionTextController.dispose();
    _nameTextController.dispose();
    _urlTextController.dispose();
    super.dispose();
  }
}
