import 'package:flutter/material.dart';

import 'big_add_page.dart';
import 'small_add_page.dart';

class AddPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        if (constraints.maxWidth < 1200 || constraints.maxHeight < 800) {
          return SmallAddPage();
        } else {
          return BigAddPage();
        }
      },
    );
  }
}
