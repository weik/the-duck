import 'package:flutter/material.dart';

class SmallCustomTextField extends StatelessWidget {
  SmallCustomTextField({
    this.controller,
    this.labelText,
    this.maxLines,
    this.hintText,
    this.maxLength,
    this.validator,
    this.enabled,
  });

  final TextEditingController controller;
  final int maxLines;
  final String labelText;
  final String hintText;
  final int maxLength;
  final String Function(String value) validator;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: //constraints,
          BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width * (70 / 100),
        maxHeight: 150,
      ),
      margin: EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 10,
      ),
      child: TextFormField(
        enabled: enabled,
        validator: validator,
        controller: controller,
        maxLines: maxLines,
        maxLength: maxLength,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(15),
          labelText: labelText,
          hintText: hintText,
          border: OutlineInputBorder(),
          isDense: true,
        ),
      ),
    );
  }
}
