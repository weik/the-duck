import 'package:flutter/material.dart';

class DrawerCard extends StatelessWidget {
  final String title;
  final Function onTap;
  final String subtitle;

  DrawerCard({
    @required this.onTap,
    this.title,
    this.subtitle,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 15),
      child: Card(
        color: Colors.blueGrey[800],
        child: ListTile(
          title: Text(
            title,
            textAlign: TextAlign.start,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 17,
            ),
          ),
          subtitle: Text(
            subtitle,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w600,
            ),
          ),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: onTap,
        ),
      ),
    );
  }
}
