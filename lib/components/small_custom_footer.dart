import 'package:flutter/material.dart';

class SmallCustomFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      decoration: BoxDecoration(
        color: Colors.red[800].withOpacity(0.7),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          'Copyright © 2020 The Duck Team. \nAll rights reserved.',
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
