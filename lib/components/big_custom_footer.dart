import 'package:flutter/material.dart';

class BigCustomFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.topRight,
            stops: [
              0.0,
              0.3,
              0.7,
              1.0,
            ],
            colors: [
              Colors.redAccent[700].withOpacity(0.3),
              Colors.black12.withOpacity(0.2),
              Colors.black12.withOpacity(0.2),
              Colors.redAccent[700].withOpacity(0.3),
            ]),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          'Copyright © 2020 The Duck Team. \nAll rights reserved.',
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
