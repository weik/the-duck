import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../routing/router.dart';
import 'drawer_divider.dart';
import 'drawer_card.dart';

class EndDrawer extends StatelessWidget {
  final String _url = 'http://pnrtscr.com/kqrkc7';

  final GlobalKey<NavigatorState> navigatorKey;
  EndDrawer({this.navigatorKey});

  void _showMainSupportTeam() async {
    if (await canLaunch(_url)) {
      await launch(
        _url,
        forceSafariVC: false,
        forceWebView: false,
      );
    } else {
      print('can\'t launch $_url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blueGrey[800],
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(7),
                    bottomRight: Radius.circular(7),
                  ),
                ),
                child: Stack(
                  alignment: AlignmentDirectional.centerEnd,
                  children: [
                    Container(
                      alignment: AlignmentDirectional.centerStart,
                      padding: EdgeInsets.all(6.0),
                      child: GestureDetector(
                        onTap: () {
                          navigatorKey.currentState.pushNamed(home_page);
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 80,
                          width: 80,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/ducklogo.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'The Duck Team',
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          'We are happy that \nexactly YOU are here!!',
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  DrawerDivider(color: Colors.teal),
                  DrawerCard(
                    title: 'Add Page',
                    onTap: () {
                      navigatorKey.currentState.pushNamed(add_page);
                      Navigator.pop(context);
                    },
                    subtitle: 'Your contribution = Our happiness',
                  ),
                  DrawerDivider(color: Colors.brown),
                  DrawerCard(
                    title: 'About Us',
                    onTap: () {
                      navigatorKey.currentState.pushNamed(about_us_page);
                      Navigator.pop(context);
                    },
                    subtitle: 'Want to see THE Developers?!',
                  ),
                  DrawerDivider(color: Colors.brown),
                  DrawerCard(
                    title: 'Support Page',
                    onTap: () {
                      navigatorKey.currentState.pushNamed(support_page);
                      Navigator.pop(context);
                    },
                    subtitle: 'Are you a F***ING bug hunter?',
                  ),
                  DrawerDivider(color: Colors.brown),
                ],
              ),
              SizedBox(
                height: 1,
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: FlatButton(
              child: Text(
                'Our support mailings, be careful ^-^',
              ),
              hoverColor: Colors.black12.withOpacity(0),
              focusColor: Colors.black12.withOpacity(0),
              textColor: Colors.blueGrey[400],
              onPressed: _showMainSupportTeam,
            ),
          ),
        ],
      ),
    );
  }
}
