import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'screens/home_page/home_page.dart';
import 'screens/main_page/main_page.dart';
import 'provider/home_page_logic.dart';
import 'provider/support_page_logic.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => HomePageLogic(),
        ),
        ChangeNotifierProvider(
          create: (context) => SendMessageSupport(),
        ),
      ],
      child: TheDuckApp(),
    ),
  );
}

class TheDuckApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        highlightColor: Colors.deepOrange.withOpacity(0.5),
        errorColor: Colors.redAccent[400],
        accentColor: Colors.deepOrange.withOpacity(0.5),
      ),
      home: MainPage(
        child: HomePage(),
      ),
    );
  }
}
